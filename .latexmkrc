#!/usr/bin/env perl
#$dvipdf           = 'dvipdfmx  -f texfonts.map %O -o %D %S';
$latex            = 'platex -halt-on-error -file-line-error';
$makeindex        = 'mendex %O -o %D %S';
# $dvipdf           = 'dvipdfmx %O -o %D %S';
$dvipdf           = 'dvipdfmx -f texfonts.map %O -o %D %S';
$bibtex           = 'pbibtex';
$max_repeat       = 5;
$pdf_mode	  = 3; # generates pdf via dvipdfmx

$pvc_view_file_via_temporary = 0;

$pdf_previewer    = "qpdfview %S";
